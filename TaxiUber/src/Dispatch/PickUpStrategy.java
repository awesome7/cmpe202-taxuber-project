package Dispatch;

import InventorySchedule.InventorySchedule;
import InventorySchedule.VehicleAndDriver;
import Requests.Request;

import java.util.List;

/**
 * Created by hughma on 8/6/15.
 */
public class PickUpStrategy implements DispatchStrategyIntf{

    @Override
    public VehicleAndDriver dispatch(Request rq) {
        List<VehicleAndDriver> roamingList = InventorySchedule.getInstance().getActiveVehicleList();
        for (VehicleAndDriver vd : roamingList) {
            if (vd.getDriver().get_name().equals(rq.getUser().get_name())) {
                Utils.Utils.contextLog("Driver: " + vd.getDriver().get_name() + " picked up a customer");
                vd.getVehicle().getState().scheduledForService();
                return vd;
            }
        }
        Utils.Utils.contextLog("Should Never Be Here");
        return null;
    }
}
