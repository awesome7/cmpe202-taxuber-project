package Dispatch;

import InventorySchedule.InventorySchedule;
import InventorySchedule.VehicleAndDriver;
import Requests.Request;

import java.util.List;

public class DefaultStrategy implements DispatchStrategyIntf {

	@Override
	public VehicleAndDriver dispatch(Request rq) {
		List<VehicleAndDriver> cars = InventorySchedule.getInstance().getActiveVehicleList();
		for (VehicleAndDriver car : cars) {
			if (car.getVehicle().getState().toString().equalsIgnoreCase("available")) {
				Utils.Utils.contextLog("allocating car/driver:"
						+ car.getVehicle().getVinNumber() + "/" + car.getDriver().get_name());
				car.getVehicle().getState().scheduledForService();
				return car;
			}
		}
		Utils.Utils.contextLog("no cars available");
		return null;
	}

}
