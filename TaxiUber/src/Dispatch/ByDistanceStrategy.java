package Dispatch;

import InventorySchedule.InventorySchedule;
import InventorySchedule.VehicleAndDriver;
import Requests.Request;

import java.util.List;

public class ByDistanceStrategy implements DispatchStrategyIntf {

	@Override
	public VehicleAndDriver dispatch(Request rq) {

		List<VehicleAndDriver> vehicles = InventorySchedule.getInstance()
				.getActiveVehicleList();

		VehicleAndDriver closestVehicle = null;

		double lastClosest = 99999999999.99;

		for (VehicleAndDriver thisVehicle : vehicles) {
			if (thisVehicle.getVehicle().getState().toString().equalsIgnoreCase("available")) {
				double distance = rq.getStartPoint().distance(
						thisVehicle.getVehicle().getLocation());
				if (distance < lastClosest) {
					closestVehicle = thisVehicle;
					Utils.Utils.contextLog("Vehicle:"
							+ thisVehicle.getVehicle().getVinNumber() + " at "
							+ String.format("%.2f", distance) + " away");
					lastClosest = distance;
				}
			}
		}

		if (closestVehicle == null) {
			Utils.Utils.contextLog("no cars available");
		} else {
			Utils.Utils.contextLog("Picked vehecle "
					+ closestVehicle.getVehicle().getVinNumber()
					+ " at a distance:" + String.format("%.2f", lastClosest) + "miles");
			closestVehicle.getVehicle().getState().scheduledForService();
		}

		return closestVehicle;
	}

}
