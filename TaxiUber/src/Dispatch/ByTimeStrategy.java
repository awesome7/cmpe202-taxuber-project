package Dispatch;

import java.util.List;

import vehicle.Vehicle;
import InventorySchedule.InventorySchedule;
import InventorySchedule.VehicleAndDriver;
import Requests.Request;
import Users.Driver;

public class ByTimeStrategy implements DispatchStrategyIntf {

	@Override
	public VehicleAndDriver dispatch(Request rq) {
		List<Vehicle> cars = InventorySchedule.getInstance().getCompanyVehicleList();
	
		for (Vehicle car : cars) {
			if (car.isFree(rq.getTime())) {
				Utils.Utils.contextLog("Reserving car:" + car.getVinNumber());
				VehicleAndDriver vd = new VehicleAndDriver();
				vd.setDriver(new Driver(null, null, null, null, 0, 0, 0, 0)); // dummy
				vd.setVehicle(car);
				return vd;
			}
		}
		Utils.Utils.contextLog("no cars available");
		return null;
	}

}
