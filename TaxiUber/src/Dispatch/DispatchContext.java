package Dispatch;

import java.time.Duration;
import java.time.LocalDateTime;

import InventorySchedule.VehicleAndDriver;
import Requests.Request;
import Users.Driver;
import Utils.Utils;

public class DispatchContext {
	private DispatchStrategyIntf dispatchStrategy;

	public DispatchContext() {
		Utils.contextLog("DispatchContext created");
	}

	public void setDispatchStrategy(DispatchStrategyIntf ds) {
		dispatchStrategy = ds;
	}

	public VehicleAndDriver dispatch(Request rq) {

		Utils.contextLog("processing request for " + rq.getUser().get_name());
		setDispatchStrategy(getDispatchStrategy(rq));
		return dispatchStrategy.dispatch(rq);
	}

	private DispatchStrategyIntf getDispatchStrategy(Request rq) {
		
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime reqTime = rq.getTime();
		
		long duration = Duration.between(now, reqTime).getSeconds();
		if (rq.getUser() instanceof Driver) {
			Utils.contextLog("Driver: " + rq.getUser().get_name() + " reported being flagged by pedestrian");
			return new PickUpStrategy();
		} else if (duration > 3600) {
			Utils.contextLog("Picking ByTimeStrategy because the request time is after 1 hr");
			return new ByTimeStrategy();			
		} else {
			Utils.contextLog("Picking ByDistance. Closest vehicle will be picked");
			return new ByDistanceStrategy();
		}
	}
}
