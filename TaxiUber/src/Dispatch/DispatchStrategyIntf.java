package Dispatch;

import InventorySchedule.VehicleAndDriver;
import Requests.Request;

public interface DispatchStrategyIntf {
	public VehicleAndDriver dispatch(Request rq); 
}
