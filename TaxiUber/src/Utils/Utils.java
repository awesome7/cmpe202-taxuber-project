package Utils;

import Requests.Request;
import Users.Customer;
import reports.CustomerReport;

import java.util.ArrayList;

public class Utils {

	public static void rawLog(String message) {
		System.out.println(message);
	}

	public static void contextLog(String message) {
		final StackTraceElement[] stack = Thread.currentThread()
				.getStackTrace();
		rawLog(stack[stack.length - 2].getMethodName() + "() " + message);
	}

	public static void logWithIndent(int indent, String message) {
		StringBuffer sbBuffer = new StringBuffer();
		for (int i = 0; i < indent; i++) {
			sbBuffer.append(" ");
		}
		sbBuffer.append(message);
		rawLog(sbBuffer.toString());
	}

	public static void printCustomers(ArrayList<Request> requests) {
        for (Request req : requests) {
            CustomerReport customerReport = new CustomerReport((Customer) req.getUser());
            customerReport.printReport();
        }
    }

	}
