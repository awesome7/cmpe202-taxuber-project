package Requests;

import static Utils.Utils.contextLog;

/**
 * Created by hughma on 7/27/15.
 */
public class CompletedState implements RequestState {
    private Request __request;

    public CompletedState(Request request) {
        this.__request = request;
    }

    @Override
    public void stateAction() {
        contextLog("Request is Completed");
        goToNextState();
    }

    @Override
    public void goToNextState(){
        // Notify service?
    }
    
    public String stateDescription() {
    	return "Completed";
    }
}
