package Requests;

/**
 * Created by hughma on 7/27/15.
 */
public class ValidatingState implements RequestState {
    private Request __request;

    public ValidatingState(Request request) {
        this.__request = request;
        stateAction();
    }

    @Override
    public void stateAction() {
        System.out.println("Request is Validating");
        this.__request._requestRules.checkRules();
    }

    @Override
    public void goToNextState() {
        this.__request.setState(new SchedulingState(this.__request));
    }
    
    public String stateDescription() {
    	return "Validating";
    }
}
