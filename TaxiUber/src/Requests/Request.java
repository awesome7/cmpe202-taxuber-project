package Requests;

import Rules.RequestRules;
import Services.Service;
import Users.User;
import reports.RequestReport;

import java.awt.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by hughma on 7/25/15.
 */
public abstract class Request {
    protected User _user;
    protected LocalDateTime _time;
    protected String _requestId;
    protected RequestState _requestState;
    protected RequestRules _requestRules;
    protected Point _startLocation;
    protected Point _endLocation;
    protected Service _service;

    public void setState(RequestState state){
        this._requestState = state;
        printRequest(this);
    }

    public RequestState getRequestState() {
        return this._requestState;
    }

    public String getRequestId(){
        return this._requestId;
    }

    public User getUser(){
        return this._user;
    }

    public Service getService() { return this._service; }

    public void cancelService() {
        if (_service != null) {
            _service.cancelService();
        }
    }

    public void setService(Service service) { this._service = service; }

    public Point getStartPoint(){
        return this._startLocation;
    }

    public Point getEndPoint(){
        return this._endLocation;
    }

	public LocalDateTime getTime() {
		return _time;
	}
	
	public String getRequetTime() {
    	if (_time != null) {
        	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            String formattedDateTime = _time.format(formatter);
            return formattedDateTime;
        } else {
    		return "Not started";
    	}
	}

	public void setTime(LocalDateTime time) {
		this._time = time;
	}

    public void printRequest(Request r) {
    	RequestReport requestReport = new RequestReport(r);
    	requestReport.printReport();
    }
}
