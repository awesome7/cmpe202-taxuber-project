package Requests;

/**
 * Created by hughma on 7/27/15.
 */
public class ProcessingState implements RequestState {
    private Request __request;

    public ProcessingState(Request request) {
        this.__request = request;
    }

    @Override
    public void stateAction() {
        System.out.println("Request is Processing");
        goToNextState();
    }

    @Override
    public void goToNextState() {
        this.__request.setState(new CompletedState(this.__request));
    }
    
    public String stateDescription() {
    	return "Processing";
    }
}
