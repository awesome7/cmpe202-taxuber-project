package Requests;

import Services.ApprovedServiceState;

import static Utils.Utils.contextLog;

/**
 * Created by hughma on 7/27/15.
 */
public class ApprovedState implements RequestState {
    private Request __request;

    public ApprovedState(Request request) {
        this.__request = request;
        stateAction();
    }

    @Override
    public void stateAction() {
        contextLog("Request is Approved");
        __request.getService().approveService();
    }

    @Override
    public void goToNextState() {
        this.__request.setState(new CompletedState(this.__request));
    }
    
    public String stateDescription() {
    	return "Approved";
    }
}
