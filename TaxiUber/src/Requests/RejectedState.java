package Requests;

import static Utils.Utils.contextLog;

/**
 * Created by hughma on 7/27/15.
 */
public class RejectedState implements RequestState{
    private Request __request;

    public RejectedState(Request request) {
        this.__request = request;
        stateAction() ;
    }

    @Override
    public void stateAction() {
        contextLog("Request is Rejected");
        __request.cancelService();
    }

    @Override
    public void goToNextState() {
    }
    
    public String stateDescription() {
    	return "Rejected";
    }
}
