package Requests;

import Payments.CashPayment;
import Payments.CreditPayment;
import Payments.Payment;
import Services.Trip;

import java.util.Scanner;

import static Utils.Utils.contextLog;

/**
 * Created by hughma on 7/27/15.
 */
public class SchedulingState implements RequestState {
    private Request __request;

    public SchedulingState(Request request) {
        this.__request = request;
        stateAction();
    }

    @Override
    public void stateAction() {
        System.out.println("Request is Validated");
        System.out.println("Request is Scheduling");

        Scanner scan = new Scanner(System.in);
        String payType ;
        Payment reqPay = null;

        while(reqPay == null) {
            System.out.println("How would you like to pay for your service?");
            System.out.println("Press 1 for Cash or Press 2 for Credit");
            payType = scan.next();
            if (payType.equals("1")) {
                reqPay = new CashPayment(__request.getUser().get_name());
            } else if (payType.equals("2")) {
                reqPay = new CreditPayment(__request.getUser().get_name());
            } else {
                System.out.println("Please try again, value must be either 1 or 2");
            }
        }

        System.out.println("Request is creating new Service");

        // Set new service with current request, to the current request
        this.__request.setService(new Trip(this.__request, reqPay));
        if(this.__request.getService().getServiceState().toString().equalsIgnoreCase("scheduled")) {
            goToNextState();
        } else {
            contextLog("Service was created and is queued waiting for resources");
        }
    }

    @Override
    public void goToNextState() {
        this.__request.setState(new ApprovingState(this.__request));
    }
    
    public String stateDescription() {
    	return "Scheduling";
    }
}
