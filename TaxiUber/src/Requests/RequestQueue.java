package Requests;

import Dispatch.DispatchContext;
import InventorySchedule.InventorySchedule;
import InventorySchedule.VehicleAndDriver;
import Services.Service;
import Users.Driver;
import reports.ServiceReport;
import vehicle.ProvidingServiceState;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;

import static Utils.Utils.contextLog;

/**
 * Created by jasroger on 8/8/15.
 */
public class RequestQueue {
    private static RequestQueue instance = null;
    protected ArrayDeque<Request> _requestQueue;

    private RequestQueue() {
        _requestQueue = new ArrayDeque<>();
    }

    public static synchronized RequestQueue getInstance() {
        if (instance == null) {
            instance = new RequestQueue();
        }
        return instance;
    }

    public void addRequest(Request req) {
            _requestQueue.add(req);
            contextLog("Added Request to the Queue");
    }

    public void processQueue() {
        Request r ;
        while (_requestQueue.size() != 0) {
            r = _requestQueue.getFirst();
            r.setState(new ValidatingState(r));
            _requestQueue.remove(r);
        }
    }

    public Request getNextRequest() {
        return _requestQueue.getFirst();
    }

    //public ArrayList<RequestReport> getRequestReportList() {
    //    return requestReportList;
    //}

    public void printContents() {
        contextLog("Request Queue Holds " + _requestQueue.size()
                + " Services Waiting To Be Allocated");
    }
}
