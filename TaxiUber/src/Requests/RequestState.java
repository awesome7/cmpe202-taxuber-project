package Requests;

/**
 * Created by hughma on 7/25/15.
 */
public interface RequestState {

    void stateAction();
    void goToNextState();
    String stateDescription();
}
