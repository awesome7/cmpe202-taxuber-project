package Requests;

import Users.Driver;

import java.util.Scanner;

/**
 * Created by hughma on 8/2/15.
 */
public class ApprovingState implements RequestState {
    private Request __request;

    public ApprovingState(Request request) {
        this.__request = request;
        stateAction();
    }

    @Override
    public void stateAction() {
        if(__request.getUser() instanceof Driver) {
            System.out.println("Driver has stopped next to you, will you get in the car? (Yes/No)");
        }else {
            System.out.println("Request is ready to be scheduled, do you approve? (Yes/No)");
        }
        Scanner scan = new Scanner(System.in);
        String _approval = scan.next();

       if (_approval.equalsIgnoreCase("Yes")) {
            goToNextState();
        }
        else {
            this.__request.setState(new RejectedState(this.__request));
        }

        // goToNextState();
    }

    @Override
    public void goToNextState() {
        this.__request.setState(new ApprovedState(this.__request));
    }
    
    public String stateDescription() {
    	return "Approving";
    }
}
