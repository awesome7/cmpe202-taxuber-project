package Requests;

import java.awt.Point;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

import Rules.RequestRules;
import Users.User;

public class ManualRequest extends Request {

	    public ManualRequest(User user, Point startLocation, Point endLocation){
	        this._user = user;
	        this._startLocation = startLocation;
	        this._endLocation = endLocation;
	        this._requestRules = new RequestRules(this);

	        Date date = new Date();
	        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy h:mm:ss a");
	        sdf.format(date);
	        this._time = LocalDateTime.now();
	        this._time = this._time.plusHours(2);

	        this.setState(new QueuingState(this));

	    }
}
