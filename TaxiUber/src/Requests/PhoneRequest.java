package Requests;

import Rules.RequestRules;
import Users.User;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Created by hughma on 8/7/15.
 */
public class PhoneRequest extends Request {

    public PhoneRequest(User user, Point startLocation, Point endLocation) {
        this._user = user;
        this._startLocation = startLocation;
        this._endLocation = endLocation;
        this._requestRules = new RequestRules(this);

        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy h:mm:ss a");
        sdf.format(date);
        this._time = LocalDateTime.now();

        this.setState(new QueuingState(this));

    }
}
