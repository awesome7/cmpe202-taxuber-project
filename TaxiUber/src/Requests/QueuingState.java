package Requests;

/**
 * Created by hughma on 7/27/15.
 */
public class QueuingState implements RequestState {
    private Request __request;

    public QueuingState(Request request) {
        this.__request = request;
        stateAction();
    }

    @Override
    public void stateAction() {
        System.out.println("Request is Queuing");
    }

    @Override
    public void goToNextState() {
        this.__request.setState(new ValidatingState(this.__request));
    }
    
    public String stateDescription() {
    	return "Queuing";
    }
}
