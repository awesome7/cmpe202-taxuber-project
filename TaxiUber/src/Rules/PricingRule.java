package Rules;

import Pricing.PricingStrategy;

/**
 * Created by jasroger on 8/6/15.
 */
public class PricingRule implements Rules {

    protected float _pricePerMile;
    protected float _pricePerMinute;

    public PricingRule() {
        _pricePerMile = 1.15f;
        _pricePerMinute = 1.55f ;
    }

    @Override
    public void checkRules() {
    }

    public float getPricePerMile() {
        return _pricePerMile ;
    }
    public float getPricePerMinute() {
        return _pricePerMinute ;
    }
}
