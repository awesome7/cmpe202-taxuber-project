package Payments;

import reports.PaymentReport;

/**
 * Created by jasroger on 7/28/15.
 */
public class CashPayment extends Payment {

    public CashPayment(String payerName) {
        _payerName = payerName ;
    }

    @Override
    public void setupPayment() {
        System.out.println("Driver has indicated cash payment method");
    }

    @Override
    public void processPayment() {
        System.out.format("Customer %s paid cash in the amount of $%.2f for services\n", _payerName, _paymentAmount);
        _paymentSucceeded = true ;
    }

    @Override
    public void printReciept() {
    	PaymentReport cashPaymentReport = new PaymentReport(this, "Cash");
    	cashPaymentReport.printReport();
    }
}
