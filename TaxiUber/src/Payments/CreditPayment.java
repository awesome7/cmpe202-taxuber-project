package Payments;

import java.util.Scanner;

import reports.PaymentReport;

/**
 * Created by jasroger on 7/28/15.
 */
public class CreditPayment extends Payment {

    private String _ccNumber ;
    private String _ccExpiration ;

    public CreditPayment(String payerName) {
        _payerName = payerName ;
    }

    @Override
    public void setupPayment() {
        System.out.println("Enter Credit Card Number - this will not be stored by our system");
        Scanner scan = new Scanner(System.in);
        _ccNumber = scan.next();
        System.out.println("Enter Credit Card Expiration Date as MM/YY");
        _ccExpiration = scan.next();

        System.out.println("Customer has entered Credit Card information, Beginning Transaction...");
    }

    @Override
    public void processPayment() {
        System.out.format("Charging customer %s's card $%.2f for services\n", _payerName, _paymentAmount);
        _paymentSucceeded = true ;
    }

    @Override
    public void printReciept() {
    	PaymentReport creditPaymentReport = new PaymentReport(this, "Credit");
    	creditPaymentReport.printReport();
    }
}
