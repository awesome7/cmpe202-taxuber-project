package Payments;


/**
 * Created by jasroger on 7/28/15.
 */
abstract public class Payment {
    protected float _paymentAmount ;
    protected String _payerName ;
    protected boolean _paymentSucceeded ;

    public void completeTransaction() {
        _paymentSucceeded = false;
        while ( !_paymentSucceeded ) {
            setupPayment();
            processPayment();
        }
        printReciept();
    }

    abstract public void setupPayment();
    abstract public void processPayment();
    abstract public void printReciept();

    public void setAmount(float amt) {
        _paymentAmount = amt ;
    }

    public float getAmount() {
        return _paymentAmount ;
    }
    
    public String getPaymentStatus() {
    	if (_paymentSucceeded == true) {
    		return "Succeed";
    	}
    	else {
    		return "Failed";
    	}
    }
}
