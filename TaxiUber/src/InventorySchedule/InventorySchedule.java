package InventorySchedule;

import Users.Driver;
import Utils.Utils;
import reports.VehicleAndDriverReport;
import vehicle.CompanyOwned;
import vehicle.DriverOwned;
import vehicle.Vehicle;
import vehicle.VehicleCar;

import java.awt.*;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class InventorySchedule {
	private static InventorySchedule instance = null;
	private static ArrayList<VehicleAndDriver> activeVehicles = null;
	private static ArrayList<Vehicle> companyVehicles = null;
	private static ArrayList<VehicleAndDriverReport> inventoryReportList;

	private InventorySchedule() {
		activeVehicles = new ArrayList<VehicleAndDriver>();
		companyVehicles = new ArrayList<Vehicle>();
		inventoryReportList = new ArrayList<VehicleAndDriverReport>();
	}

	public static synchronized InventorySchedule getInstance() {
		if (instance == null) {
			instance = new InventorySchedule();
		}
		return instance;
	}

	public List<VehicleAndDriver> getActiveVehicleList() {
		return activeVehicles;
	}

	public List<Vehicle> getCompanyVehicleList() {
		return companyVehicles;
	}

	public ArrayList<VehicleAndDriverReport> getInventoryReportList() {
		return inventoryReportList;
	}

	public VehicleAndDriver getFreeVehicleAndDriver() {
		for (VehicleAndDriver vd : activeVehicles) {
			if (vd.getVehicle().getState().toString().equalsIgnoreCase("available")) {
				return vd;
			}
		}
		return null;
	}

	public void loadObjectsForTesting(int objCount) {
		Utils.rawLog("Loading driver owned vehicles...\n");
		Random rand = new Random(LocalTime.now().toNanoOfDay());
		
		for (int i = 0; i < objCount; i++) {
			VehicleAndDriver vd = new VehicleAndDriver();

			vd.setDriver(new Driver("Driver-" + i, "phone#", "lic#", "insur#",
					10, 0, 4, 0));

			VehicleCar c = new VehicleCar("vin#" + i, "make-x", "model-x",
					new DriverOwned("driver#" + i));
			c.setLocation(new Point(rand.nextInt(100), rand.nextInt(100)));
			vd.setVehicle(c);

			InventorySchedule.getInstance().addVehicleAndDriver(vd);
			reportAddDriverOwnedInventory(vd);
		}

		Utils.rawLog("Loading company owned vehicles...\n");
		for (int i = 0; i < objCount; i++) {
			VehicleAndDriver vd = new VehicleAndDriver();
			vd.setDriver(new Driver("CompanyDriver-" + i, "phone#", "lic#",
					"insur#", 10, 0, 4, 0));
			Vehicle v = new VehicleCar("company#" + i, "make#", "model#",
					new CompanyOwned("company"));
			v.setLocation(new Point(50, 50));// right in the middle
			vd.setVehicle(v);


			InventorySchedule.getInstance().addVehicleAndDriver(vd);
			InventorySchedule.getInstance().addVehicle(v);
			reportAddCompanyOwnedInventory(vd);
		}
	}

	public synchronized void addVehicleAndDriver(VehicleAndDriver vd) {
		activeVehicles.add(vd);
	}

	public synchronized void addVehicle(Vehicle v) {
		companyVehicles.add(v);
	}

	public void reportAddDriverOwnedInventory(VehicleAndDriver vd) {
		VehicleAndDriverReport vehicleAndDriverReport = new VehicleAndDriverReport(
				vd);
		System.out.println("Add driver owned inventory:");
		System.out
				.println("---------------------------------------------------");
		inventoryReportList.add(vehicleAndDriverReport);
		vehicleAndDriverReport.printReport();
		System.out
				.println("---------------------------------------------------");
		System.out.println("");
	}

	public void reportAddCompanyOwnedInventory(VehicleAndDriver vd) {
		VehicleAndDriverReport vehicleAndDriverReport = new VehicleAndDriverReport(
				vd);
		System.out.println("Add company owned inventory:");
		System.out
				.println("---------------------------------------------------");
		inventoryReportList.add(vehicleAndDriverReport);
		vehicleAndDriverReport.printReport();
		System.out
				.println("---------------------------------------------------");
		System.out.println("");
	}
}
