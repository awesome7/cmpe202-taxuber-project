package InventorySchedule;

import Users.Driver;
import vehicle.Vehicle;

public class VehicleAndDriver {
	private Vehicle vehicle;
	private Driver driver;

	public Vehicle getVehicle() {
		return vehicle;
	}
	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	public Driver getDriver() {
		return driver;
	}
	public void setDriver(Driver driver) {
		this.driver = driver;
	}	
}
