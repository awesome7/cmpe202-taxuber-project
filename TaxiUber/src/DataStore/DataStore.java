package DataStore;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by hughma on 7/28/15.
 */
public class DataStore {
    private HashMap<Class<?>, ArrayList> __dataStore = new HashMap<>();

    public DataStore() {}

    // Store object to their appropriate HashMap key
    // object gets casted to proper class before being added
    @SuppressWarnings("unchecked")
    public <T> void put(Class<T> key, T value) {
        if(!contains(key)) {
            this.__dataStore.put(key, new ArrayList<T>());
        }
        this.__dataStore.get(key).add(value.getClass().cast(value));
    }

    public <T> boolean contains(Class<T> key) {
        return this.__dataStore.containsKey(key);
    }

    // Returns Array list of Class generic class type
    @SuppressWarnings("unchecked")
    public <T> ArrayList<T> get(Class <T> clazz) {
        return this.__dataStore.get(clazz);
    }

}
