package Services;

import java.time.LocalDateTime;

import static Utils.Utils.contextLog;

/**
 * Created by jasroger on 7/30/15.
 */
public class ApprovedServiceState implements ServiceState {

    protected Service _service ;

    public ApprovedServiceState(Service serv) {
        _service = serv ;
    }

    public void scheduleService() {
        contextLog("Can't Schedule a Service that's already scheduled");
    }

    public void approveService() {
        contextLog("Can't Approve a Service that's already approved");
    }

    public void startService() {
       contextLog("Starting Service");
        _service._startPoint = _service.getRequest().getStartPoint();
        _service._endPoint = _service.getRequest().getEndPoint();
        _service._startTime = LocalDateTime.now();
        _service.setServiceState(new StartedServiceState(_service)) ;
    }
    public void completeService() {
        contextLog("Can't Complete a Service that isn't started");

    }
    public void payService() {
        contextLog("Can't Pay a Service that isn't completed");
    }

    public void cancelService() {
        _service.setServiceState(new CancelledServiceState(_service)) ;
    }

    @Override
    public String toString() {
        return "approved";
    }
}
