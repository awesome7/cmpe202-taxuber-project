package Services;

import Users.Customer;
import vehicle.AvailableState;
import vehicle.Vehicle;
import Users.Driver;

import static Utils.Utils.contextLog;

/**
 * Created by jasroger on 7/27/15.
 */
public class PaidServiceState implements ServiceState {

	protected Service _service;

	public PaidServiceState(Service serv) {
		_service = serv;
		freeVehicleAndDriver();
		updateCustomerStats();
	}

	public void scheduleService() {
		contextLog("Can't Schedule a Service that's already scheduled");
	}

	public void approveService() {
		contextLog("Can't Approve a Service that's already approved");
	}
	public void startService() {
		contextLog("Can't Start a Service that's already started");
	}
	public void completeService() {
		contextLog("Can't Complete a Service that's already completed");
	}
	public void payService() {
		contextLog("Can't Pay a Service that's already completed");
	}
	public void cancelService() {
		contextLog("Can't Cancel a Service that's already completed");
	}

	public void freeVehicleAndDriver() {
		contextLog("Service LifeCycle Is Completed");
		contextLog("De-allocating vehicle/driver - "
				+ _service.getVehicleAndDriver().getVehicle().getVinNumber()
				+ "/" + _service.getVehicleAndDriver().getDriver().get_name());
		Vehicle v = _service.getVehicleAndDriver().getVehicle();
		v.setVehicleState(new AvailableState(v));
		contextLog("Set the vehicle location to the end point of request");
		v.setLocation(_service.getRequest().getEndPoint());
		contextLog("Set the vehicle state back to available");
		_service.getVehicleAndDriver().getVehicle().getState().available();
	}

	public void updateCustomerStats() {
		if (!(_service.getRequest().getUser() instanceof Driver)){
			((Customer) _service.getRequest().getUser()).addRide();
			((Customer) _service.getRequest().getUser()).addMiles(_service.getDistanceInMiles());
		}
	}

	@Override
	public String toString() {
		return "paid";
	}
}
