package Services;

import Requests.ApprovingState;

import static Utils.Utils.contextLog;

/**
 * Created by jasroger on 7/27/15.
 */
public class QueueingServiceState implements ServiceState {

    protected Service _service ;

    public QueueingServiceState(Service serv) {
        contextLog("Service is in Queueing State, adding to ServiceQueue.");
        _service = serv ;
        _service.addToQueue();
    }

    public void scheduleService() {
        _service.setServiceState(new SchedulingServiceState(_service)) ;
        _service.getRequest().setState(new ApprovingState(_service.getRequest()));

    }
    public void approveService() {
        contextLog("Can't Approve a Service that isn't scheduled");
    }
    public void startService() {
        contextLog("Can't Start a Service that isn't approved");
    }
    public void completeService() {
        contextLog("Can't Complete a Service that isn't started");

    }
    public void payService() {
        contextLog("Can't Pay a Service that isn't completed");
    }
    public void cancelService() {
        _service.setServiceState(new SchedulingServiceState(_service)) ;
    }

    @Override
    public String toString() {
        return "queueing";
    }
}
