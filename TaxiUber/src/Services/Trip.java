package Services;

import Payments.Payment;
import Requests.Request;

import java.awt.*;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneOffset;

/**
 * Created by jasroger on 7/27/15.
 */
public class Trip extends Service {

    public Trip(Request req, Payment pay) {
        super(req, pay);
        _distance = 0;
    }

    @Override
    public void setDistance() {
        if (_endPoint != null && _startPoint != null) {
            _distance = (int) (Math.abs(_endPoint.getX() - _startPoint.getX()) + Math.abs(_endPoint.getY() - _startPoint.getY()));
        }
    }

    @Override
    public void setStartPoint(Point start) {
        _startPoint = start ;
    }

    @Override
    public void setEndPoint(Point end) {
        _endPoint = end ;
    }

    @Override
    public long getDistanceInMiles() {
        return _distance;
    }

    @Override
    public long getTotalTimeInMinutes() {
        Instant startI = _startTime.toInstant(ZoneOffset.UTC);
        Instant endI = _finishTime.toInstant(ZoneOffset.UTC);
        long tminus = Duration.between(startI, endI).toMinutes();
        return tminus ;
    }

    @Override
    public float getBidPricePerMile() {
        return 0;
    }


    public void cancelService() {
        setServiceState(new CancelledServiceState(this));
        if (_vehicleAndDriver != null) {
            _vehicleAndDriver.getDriver().removeService();
            _vehicleAndDriver.getVehicle().getState().available();
        }
    }


}

