package Services;

import Requests.ApprovingState;

import static Utils.Utils.contextLog;

/**
 * Created by jasroger on 7/27/15.
 */
public class SchedulingServiceState implements ServiceState {

    protected Service _service ;

    public SchedulingServiceState(Service serv) {
        _service = serv ;
    }

    public void scheduleService() {
        contextLog("Can't Schedule a Service that's already scheduled");
    }
    public void approveService() {
        contextLog("Service Is Approved.");
        _service.setServiceState(new ApprovedServiceState(_service)) ;
    }
    public void startService() {
        contextLog("Can't Start a Service that isn't approved");
    }
    public void completeService() {
        contextLog("Can't Complete a Service that isn't started");

    }
    public void payService() {
        contextLog("Can't Pay a Service that isn't completed");
    }
    public void cancelService() {
        _service.setServiceState(new CancelledServiceState(_service)) ;
    }

    @Override
    public String toString() {
        return "scheduled";
    }
}
