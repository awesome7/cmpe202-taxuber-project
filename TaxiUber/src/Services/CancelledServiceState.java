package Services;

import static Utils.Utils.contextLog;

/**
 * Created by jasroger on 7/27/15.
 */
public class CancelledServiceState implements ServiceState{

    protected Service _service ;

    public CancelledServiceState(Service serv) {
        _service = serv ;
        if (_service._vehicleAndDriver != null) {
            _service.getVehicleAndDriver().getDriver().removeService();
            _service.getVehicleAndDriver().getVehicle().getState().available();
        }
        contextLog("Service Has Been Cancelled");
    }

    public void scheduleService() {
        contextLog("Can't Schedule a Service that's cancelled");
    }

    public void approveService() {
        contextLog("Can't Approve a Service that's cancelled");
    }
    public void startService() {
        contextLog("Can't Start a Service that's cancelled");
    }
    public void completeService() {
        contextLog("Can't Complete a Service that's cancelled");
    }
    public void payService() {
        contextLog("Can't Pay a Service that's cancelled");
    }
    public void cancelService() {
        contextLog("Can't Cancel a Service that's already cancelled");
    }

    @Override
    public String toString() {
        return "cancelled";
    }
}
