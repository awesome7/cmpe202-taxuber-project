package Services;

/**
 * Created by jasroger on 8/1/15.
 */
public interface ServiceObserver {
    public void update(Service server, Object args);
}
