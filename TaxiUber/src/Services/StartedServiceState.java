package Services;

import java.time.LocalDateTime;
import java.util.Random;

import static Utils.Utils.contextLog;

/**
 * Created by jasroger on 7/27/15.
 */
public class StartedServiceState implements ServiceState {

    protected Service _service ;

    public StartedServiceState(Service serv) {
        _service = serv ;
    }
    public void scheduleService() {
        contextLog("Can't Schedule a Service that's already scheduled");
    }

    public void approveService() {
        contextLog("Can't Approve a Service that's already approved");
    }
    public void startService() {
        contextLog("Can't Start a Service that's already started");
    }
    public void completeService() {
        Random r = new Random();
        _service.setDistance();
        long d = _service.getDistanceInMiles();
        int slowfactor = r.nextInt(4) + 1;
        _service._finishTime = LocalDateTime.now().plusMinutes(d * slowfactor);
        _service.setServiceState(new CompletedServiceState(_service)) ;
        _service.payService();
    }
    public void payService() {
        contextLog("Can't Pay a Service that isn't completed");
    }
    public void cancelService() {
        contextLog("Can't Cancel a Service that is already started");
    }

    @Override
    public String toString() {
        return "started";
    }
}
