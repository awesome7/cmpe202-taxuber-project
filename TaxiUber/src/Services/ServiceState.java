package Services;

/**
 * Created by jasroger on 7/27/15.
 */
public interface ServiceState {

    public void scheduleService();
    public void approveService();
    public void startService();
    public void completeService();
    public void payService();
    public void cancelService();

    //public void doStateAction();
    //public void goToNextState();
}
