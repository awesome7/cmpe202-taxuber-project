package Services;

import Pricing.PricingContext;
import Pricing.PricingStrategy;

import static Utils.Utils.contextLog;

/**
 * Created by jasroger on 7/27/15.
 */
public class CompletedServiceState implements ServiceState {

    protected Service _service ;

    public CompletedServiceState(Service serv) {
        _service = serv ;
        contextLog("Service Completed");
    }
    public void scheduleService() {
        contextLog("Can't Schedule a Service that's already scheduled");
    }

    public void approveService() {
        contextLog("Can't Approve a Service that's already approved");
    }
    public void startService() {
        contextLog("Can't Start a Service that's already started");
    }
    public void completeService() {
        contextLog("Can't Complete a Service that's already completed");
    }
    public void payService() {
        _service.setServicePrice();
        _service.makePayment();
        _service.setServiceState(new PaidServiceState(_service)) ;
    }
    public void cancelService() {
        contextLog("Can't Cancel a Service that's already completed");
    }

    @Override
    public String toString() {
        return "completed";
    }
}
