package Services;

import Dispatch.DispatchContext;
import InventorySchedule.InventorySchedule;
import InventorySchedule.VehicleAndDriver;
import Users.Driver;
import reports.ServiceReport;
import vehicle.ProvidingServiceState;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;

import static Utils.Utils.contextLog;

/**
 * Created by jasroger on 8/1/15.
 */
public class ServiceQueue {
	private static ServiceQueue instance = null;
	protected ArrayDeque<Service> _serviceQueue;
	protected ArrayList<ServiceReport> servicesReportList;
	protected DispatchContext _dispatcher;

	private ServiceQueue() {
		_serviceQueue = new ArrayDeque<>();
		_dispatcher = new DispatchContext();
		servicesReportList = new ArrayList<ServiceReport>();
	}

	public static synchronized ServiceQueue getInstance() {
		if (instance == null) {
			instance = new ServiceQueue();
		}
		return instance;
	}

	public void addService(Service serv) {
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime reqTime = serv.getRequest().getTime();

		long duration = Duration.between(now, reqTime).getSeconds();

		if(serv.getRequest().getUser() instanceof Driver) {
			List<VehicleAndDriver> vdList = InventorySchedule.getInstance().getActiveVehicleList();
			for(VehicleAndDriver vd : vdList) {
				if (vd.getDriver().get_name().equals(serv.getRequest().getUser().get_name())){
					_serviceQueue.add(serv);
					return;
				}
			}
		}

		if (duration > 3600) {
			VehicleAndDriver vd = _dispatcher.dispatch(serv.getRequest());
			vd.getVehicle().addScheduledService(serv);
			contextLog("Reserved vehicle for the service: vin:"
					+ vd.getVehicle().getVinNumber());
		} else {
			_serviceQueue.add(serv);
			boolean new_service = true ;
			for(ServiceReport sr : servicesReportList) {
			    if (sr.getService() == serv) { new_service = false; }
			}
			if (new_service) {
				servicesReportList.add(new ServiceReport(serv));
			}
			contextLog("Added Service to Queue");
		}
	}

	public void processQueue() {
		VehicleAndDriver vehicleAndDriver;
		Service s;
		while (_serviceQueue.size() != 0) {
			s = _serviceQueue.getFirst();
			vehicleAndDriver = _dispatcher.dispatch(s.getRequest());
			if (vehicleAndDriver != null) {
				contextLog("Dispatch Successful");
				contextLog("Name of Driver is " + vehicleAndDriver.getDriver().get_name());
				vehicleAndDriver.getVehicle().setVehicleState(new ProvidingServiceState(vehicleAndDriver.getVehicle()));
				s.setVehicleAndDriver(vehicleAndDriver);
				s.scheduleService();
				_serviceQueue.remove(s);
			} else {
				contextLog("Dispatch unSuccessful");
				break;
			}
		}
	}

	public Service getNextService() {
		return _serviceQueue.getFirst();
	}
	
	public ArrayList<ServiceReport> getServicesReportList() {
		return servicesReportList;
	}

	public void printContents() {
		contextLog("Service Queue Holds " + _serviceQueue.size()
				+ " Services Waiting To Be Allocated");
	}
}
