package reports;

import java.util.ArrayList;

public class ServicesReport extends CompositeReport<ServiceReport> {

	public ServicesReport(ArrayList<ServiceReport> v) {
		super(v);
		setReportDescription("Services Report");
	}

}