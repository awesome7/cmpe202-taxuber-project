package reports;
import Requests.Request;
import Users.Customer;
import Users.Driver;

public class RequestReport extends Report {
	private Request request;

	public RequestReport(Request r) {
		request = r;
		setReportDescription("Request Report");
	}

	public void printReport() {
		System.out.println("");
		System.out.println(reportDescription);
		System.out.println("---------------------------------------------------");
		//System.out.println("Request ID:          " + request.getRequestId());
		System.out.println("Request State:       " + request.getRequestState().stateDescription());
		System.out.println("Request Start Point: " + request.getStartPoint().x + "," + request.getStartPoint().y);
		System.out.println("Request End Point:   " + request.getEndPoint().x + "," + request.getEndPoint().y);
		System.out.println("Request Time:        " + request.getRequetTime());
		
		if (request.getUser() != null) {
			if (request.getUser() instanceof Driver) {
				DriverReport driverReport = new DriverReport((Driver)request.getUser());
				driverReport.printDriver();
			}
			if (request.getUser() instanceof Customer) {
				CustomerReport driverReport = new CustomerReport((Customer)request.getUser());
				driverReport.printCustomer();
			}
		}
		
		
		if (request.getService() != null) {
			ServiceReport serviceReport = new ServiceReport(request.getService());
			serviceReport.printReport();
			System.out.println("---------------------------------------------------");
		}
		System.out.println("---------------------------------------------------");
	}
	

}
