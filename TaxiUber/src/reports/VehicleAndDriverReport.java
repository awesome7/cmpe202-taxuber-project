package reports;

import InventorySchedule.VehicleAndDriver;

public class VehicleAndDriverReport extends Report {
	private VehicleAndDriver vehicleAndDriver;
	
	public VehicleAndDriverReport(VehicleAndDriver vd) {
		vehicleAndDriver = vd;
		setReportDescription("Vehicle and Driver Report");
	}
	
	@Override
	public void printReport() {
		if (vehicleAndDriver.getDriver() != null) {
			VehicleReport vehicleReport = new VehicleReport(vehicleAndDriver.getVehicle());
			vehicleReport.printVehicle();
		}
		
		if (vehicleAndDriver.getDriver() != null) {
			DriverReport driverReport = new DriverReport(vehicleAndDriver.getDriver());
			driverReport.printDriver();
		}


		if (vehicleAndDriver.getVehicle().getState().toString().equalsIgnoreCase("available")) {
			System.out.println("Availability:      Free");
		}
		else {
			System.out.println("Availability:      Not free\n");
		}
	}
	
}
