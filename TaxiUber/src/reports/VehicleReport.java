package reports;

import vehicle.Vehicle;

public class VehicleReport extends Report {
	private Vehicle vehicle;
	
	public VehicleReport(Vehicle v) {
		vehicle = v;
		setReportDescription("Vehicle Report:");
	}
	
	private String getLocationString(){
		return "" + vehicle.getLocation().x + ", " + vehicle.getLocation().y;
	}
	
	@Override
	public void printReport() {
		System.out.println("");
		System.out.println(reportDescription);
		System.out.println("---------------------------------------------------");
		System.out.println("Vehicle ID:        " + vehicle.getVehicleId());
		System.out.println("Vehicle VinNumber: " + vehicle.getVinNumber());
		System.out.println("Vehicle Location : " + getLocationString());
		System.out.println("Vehicle Make:      " + vehicle.getVehicleMake());
		System.out.println("Vehicle Model:     " + vehicle.getVehicleModel());
		System.out.println("Vehicle Year:      " + vehicle.getVehicleYear());
		System.out.println("Vehicle Type:      " + vehicle.getVehicleType());
		System.out.println("Vehicle Seats:     " + vehicle.getNumberOfSeats());
		System.out.println("Vehicle Owner:     " + vehicle.getVehicleOwnership().getOwnerName());
		System.out.println("---------------------------------------------------");
	}
	
	public void printVehicle() {
		System.out.println("Vehicle ID:        " + vehicle.getVehicleId());
		System.out.println("Vehicle VinNumber: " + vehicle.getVinNumber());
		System.out.println("Vehicle Location : " + getLocationString());
		System.out.println("Vehicle Make:      " + vehicle.getVehicleMake());
		System.out.println("Vehicle Model:     " + vehicle.getVehicleModel());
		System.out.println("Vehicle Year:      " + vehicle.getVehicleYear());
		System.out.println("Vehicle Type:      " + vehicle.getVehicleType());
		System.out.println("Vehicle Seats:     " + vehicle.getNumberOfSeats());
		System.out.println("Vehicle Owner:     " + vehicle.getVehicleOwnership().getOwnerName());		
	}
	
}
