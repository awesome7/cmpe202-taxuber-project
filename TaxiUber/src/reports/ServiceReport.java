package reports;

import Services.Service;

import java.awt.*;

public class ServiceReport extends Report {
	private Service service;

	public ServiceReport(Service s ) {
		service = s;
		setReportDescription("Service Report");
	}

	public Service getService() {
		return service ;
	}
	
	public void printReport() {
		String startTime = service.getStartTime();
		String finishTime = service.getFinishTime();
		Point startPoint = service.getStartPoint();
		Point endPoint = service.getEndPoint();
		float distance = service.getDistance();
		float price = service.getPrice();
		String state = service.getServiceState().toString();
		
		System.out.println("");
		System.out.println(reportDescription);
		System.out.println("---------------------------------------------------");
		System.out.println("Trip Start time:  " + startTime);
		System.out.println("Trip End time:    " + finishTime);
		System.out.println("Trip Start Point: " + startPoint.x + "," + startPoint.y);
		System.out.println("Trip End Point:   " + endPoint.x + "," + endPoint.y);
		System.out.println("Trip Distance:    " + distance);
		System.out.format("Trip Cost:        $%.2f\n",price);
		System.out.println("Trip Status:      " + state);
		System.out.println("---------------------------------------------------");

	}
}
