package reports;

import java.util.ArrayList;

public class CompositeReport<T extends Report> extends Report {
		protected ArrayList<T> reports;
		
		public CompositeReport(ArrayList<T> v) {
			reports = v;
		}
		
		public void add(T report) {
			reports.add(report);
		}
		
		public void remove(T report) {
			reports.remove(report);
		}
		
		public void printReports()  {
			for (T t: reports) {
				t.printReport();
			}
		}

		public void printReport() {
			System.out.println("");
			System.out.println(reportDescription);
			System.out.println("---------------------------------------------------");
			for (int index = 0; index < reports.size(); ++index) {
				System.out.println("#" + (index+1));
				reports.get(index).printReport();
				System.out.println("---------------------------------------------------");
			}
			System.out.println("");
		}
}