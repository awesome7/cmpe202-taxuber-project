package reports;

import Payments.Payment;

public class PaymentReport extends Report {
	private Payment payment;
	private String paymentType;
	
	public PaymentReport(Payment p, String type) {
		payment = p;
		paymentType = type;
		setReportDescription("Payment Report");
	}
	
	@Override
	public void printReport() {
		System.out.println("");
		System.out.println(reportDescription);
		System.out.println("---------------------------------------------------");
		System.out.println("Payment type:   " + paymentType);
		System.out.format("Payment amount: $%.2f\n", payment.getAmount());
		System.out.println("Payment status: " + payment.getPaymentStatus());
		System.out.println("---------------------------------------------------");
		System.out.println("");
	}

}
