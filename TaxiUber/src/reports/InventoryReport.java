package reports;

import java.util.ArrayList;

public class InventoryReport extends CompositeReport<VehicleAndDriverReport> {

	public InventoryReport(ArrayList<VehicleAndDriverReport> v) {
		super(v);
		setReportDescription("Inventory Report:");
	}

}
