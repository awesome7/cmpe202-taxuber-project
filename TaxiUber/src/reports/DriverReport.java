package reports;

import Users.Driver;

public class DriverReport extends Report {
	private Driver driver;
	
	public DriverReport(Driver d) {
		driver = d;
		setReportDescription("Driver:");
	}

	@Override
	public void printReport() {
		System.out.println("");
		System.out.println(reportDescription);
		System.out.println("---------------------------------------------------");
		System.out.println("Driver Name:       " + driver.get_name());
		System.out.println("Driver Phone#:     " + driver.get_phoneNumber());
		System.out.println("Driver License:    " + driver.getLicense());
		System.out.println("Driver Insurance:  " + driver.getInsurance());
		System.out.println("---------------------------------------------------");
	}
	
	public void printDriver() {

		System.out.println("Driver Name:       " + driver.get_name());
		System.out.println("Driver Phone#:     " + driver.get_phoneNumber());
		System.out.println("Driver License:    " + driver.getLicense());
		System.out.println("Driver Insurance:  " + driver.getInsurance());
	}
}
