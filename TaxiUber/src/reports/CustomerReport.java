package reports;

import Users.Customer;

public class CustomerReport extends Report {
	private Customer customer;
	
	public CustomerReport(Customer c) {
		customer = c;
		setReportDescription("Customer Report:");
	}

	public void printReport() {
		System.out.println("");
		System.out.println(reportDescription);
		System.out.println("---------------------------------------------------");
		System.out.println("Customer Name:               " + customer.get_name());
		System.out.println("Customer Phone Number:       " + customer.get_phoneNumber());
		System.out.println("Customer No. of Rides:       " + "" + customer.getRides());
		System.out.println("Customer No. of Total Miles: " + customer.getTotalMiles());
		System.out.println("---------------------------------------------------");
	}
	
	public void printCustomer() {
		System.out.println("\n" + "Customer Name:               " + customer.get_name());
		System.out.println("Customer Phone Number:       " + customer.get_phoneNumber());
		System.out.println("Customer No. of Rides:       " + "" + customer.getRides());
		System.out.println("Customer No. of Total Miles: " + customer.getTotalMiles());
	}
}
