package reports;

import java.util.Date;
import java.text.SimpleDateFormat;

public abstract class Report {
	protected String reportDescription;
	private String reportTimestamp;
	
	public Report() {
		reportTimestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date());
	}
	
	public abstract void printReport();
	
	public String getReportTimestamp() {
		return reportTimestamp;
	}
	
	public String getReportDescription() {
		return reportDescription;
	}
	
	public void setReportDescription(String description) {
		reportDescription = description;
	}

}
