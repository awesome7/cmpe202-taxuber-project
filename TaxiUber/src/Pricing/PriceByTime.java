package Pricing;

import Rules.PricingRule;
import Services.Service;

/**
 * Created by jasroger on 7/27/15.
 */
public class PriceByTime implements PricingStrategy {

    @Override
    public float getPrice(Service service) {
        float pricePerMinute = new PricingRule().getPricePerMinute() ;
        return service.getTotalTimeInMinutes() * pricePerMinute;
    }
}
