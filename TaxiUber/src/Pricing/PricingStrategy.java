package Pricing;
import Services.Service ;

/**
 * Created by jasroger on 7/27/15.
 */
public interface PricingStrategy {

    public float getPrice(Service service) ;

}
