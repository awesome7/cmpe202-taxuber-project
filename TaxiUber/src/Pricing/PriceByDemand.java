package Pricing;

import Services.Service;

/**
 * Created by jasroger on 7/27/15.
 */
public class PriceByDemand implements PricingStrategy {

    @Override
    public float getPrice(Service service) {
        return service.getDistanceInMiles() * service.getBidPricePerMile();
    }
}
