package Pricing;

import Services.Service;

import static Utils.Utils.contextLog;

/**
 * Created by jasroger on 8/6/15.
 */
public class PricingContext {

    protected PricingStrategy _pricingStrategy;

    public PricingContext() {

    }

    public float getPrice(Service _service) {
        float price ;
        _pricingStrategy = setPricingStrategy(_service) ;
        price = _pricingStrategy.getPrice(_service);
        return price ;
    }

    public PricingStrategy setPricingStrategy(Service _service) {
        String strategy = "Time" ;

        if (_service.getDistanceInMiles() > _service.getTotalTimeInMinutes()/2) {
            strategy = "Distance";
        }
        contextLog("Setting Pricing strategy based on "+_service.getDistanceInMiles()+" miles traveled");
        contextLog("And "+_service.getTotalTimeInMinutes()+" minutes in the Vehicle");
        contextLog("Chose "+strategy);

        if (strategy.equals("Demand")) {
            return new PriceByDemand() ;
        } else if (strategy.equals("Time")) {
            return new PriceByTime() ;
        } else if (strategy.equals("Distance")) {
            return new PriceByDistance() ;
        } else {
            //Default PricingStrategy is Distance
            return new PriceByDistance() ;
        }
    }

}
