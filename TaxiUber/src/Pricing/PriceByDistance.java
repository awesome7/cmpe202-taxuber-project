package Pricing;

import Rules.PricingRule;
import Services.Service ;

/**
 * Created by jasroger on 7/27/15.
 */
public class PriceByDistance implements PricingStrategy{
        @Override
        public float getPrice(Service service) {

            float pricePerMile = new PricingRule().getPricePerMile() ;
            return pricePerMile * service.getDistanceInMiles();
        }
}
