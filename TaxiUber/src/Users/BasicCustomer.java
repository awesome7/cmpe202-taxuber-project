package Users;

/**
 * Created by hughma on 7/30/15.
 */
public class BasicCustomer extends Customer {

    private int __rides;
    private long __totalMiles;

    public BasicCustomer(String name, String phoneNumber) {
        this._name = name;
        this._phoneNumber = phoneNumber;
        this.__rides = 0;
        this.__totalMiles = 0;
    }

    public void addRide() {
        this.__rides += 1;
    }

    public int getRides() {
        return this.__rides;
    }

    public void addMiles(long miles) {
        this.__totalMiles += miles;
    }

    public long getTotalMiles() {
        return this.__totalMiles;
    }

    public void checkBenefits() {
        System.out.println("No benefits");
    }
}
