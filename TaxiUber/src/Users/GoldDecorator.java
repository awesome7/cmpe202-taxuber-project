package Users;

/**
 * Created by hughma on 7/24/15.
 */
public class GoldDecorator extends CustomerLoyaltyDecorator {

    public GoldDecorator(Customer decoratedCustomer) {
        super(decoratedCustomer);
    }

    public void checkBenefits() {
        printLoyaltyStatus(this);
        showCondiments();
        showAmenities();
        enableLounge();
    }

    public void showCondiments() {
        System.out.println("Condiments: Steak and Beer");
    }

    public void showAmenities() {
        System.out.println("Amenities: Pillow and Blanket");
    }

    public void enableLounge() {
        System.out.println("VIP Lounge Access: Enabled");
    }

    public String toString() {
        return "Gold";
    }

}
