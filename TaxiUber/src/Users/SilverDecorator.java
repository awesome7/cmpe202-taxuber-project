package Users;

/**
 * Created by hughma on 7/24/15.
 */
public class SilverDecorator extends CustomerLoyaltyDecorator {

    public SilverDecorator(Customer decoratedCustomer) {
        super(decoratedCustomer);
    }

    public void checkBenefits() {
        printLoyaltyStatus(this);
        showCondiments();
        showAmenities();
    }

    public void showCondiments() {
        System.out.println("Condiments: Peanuts and Soda");
    }

    public void showAmenities() {
        System.out.println("Amenities: Pillow");
    }

    public String toString() {
        return "Silver";
    }

}