package Users;

import Services.Service;
import Services.ServiceObserver;

/**
 * Created by hughma on 7/22/15.
 */
public abstract class User implements ServiceObserver {
    protected String _name;
    protected String _phoneNumber;

    public String get_name(){
        return _name;
    }

    public String get_phoneNumber() {
        return _phoneNumber;
    }

    abstract public void update(Service service, Object args) ;

}