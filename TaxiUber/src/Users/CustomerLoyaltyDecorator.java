package Users;

import Services.Service;

import static Utils.Utils.contextLog;

/**
 * Created by hughma on 7/23/15.
 */
public abstract class CustomerLoyaltyDecorator extends Customer {
    protected Customer _decoratedCustomer;

    public CustomerLoyaltyDecorator(Customer decoratedCustomer) {
        _decoratedCustomer = decoratedCustomer;

    }

    public String get_name() {
        return _decoratedCustomer.get_name();
    }

    public String get_phoneNumber() {
        return _decoratedCustomer.get_phoneNumber();
    }
    public int getRides() {
        return _decoratedCustomer.getRides();
    }
    public long getTotalMiles() {
        return _decoratedCustomer.getTotalMiles();
    }
    public void addRide() {
        _decoratedCustomer.addRide();
    }
    public void addMiles(long miles) {
        _decoratedCustomer.addMiles(miles);
    }

    public void update(Service serv, Object args) {
        contextLog("Notification: Customer, you're service is now " + args.toString());
    }

    public void printLoyaltyStatus(Customer dC) {
        System.out.println("======================");
        System.out.println("Customer: " + _decoratedCustomer.get_name() + "\nLoyalty Level: " + dC.toString());
        System.out.println("========Benefits======");
    }

    public void checkBenefits() {
        _decoratedCustomer.checkBenefits();
    }
}
