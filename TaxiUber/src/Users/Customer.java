package Users;

import Services.Service;

import static Utils.Utils.contextLog;

/**
 * Created by hughma on 7/22/15.
 */
public abstract class Customer extends User {

    protected String _loyaltyStatus;
    private int __rides;
    private long __totalMiles;

    public String getLoyaltyStatus() {
        return _loyaltyStatus;
    }

   public void update(Service serv, Object args) {
        contextLog("Notification: Customer, you're service is now "+args.toString());
    }

    public void addRide() {
        this.__rides += 1;
    }

    public int getRides() {
        return this.__rides;
    }

    public void addMiles(long miles) {
        this.__totalMiles += miles;
    }

    public long getTotalMiles() {
        return this.__totalMiles;
    }
    
    public abstract void checkBenefits();
}
