package Users;

/**
 * Created by hughma on 7/23/15.
 */
public class BronzeDecorator extends CustomerLoyaltyDecorator {

    public BronzeDecorator(Customer decoratedCustomer){
        super(decoratedCustomer);

    }

    public void checkBenefits() {
        printLoyaltyStatus(this);
        showCondiments();
    }

    public void showCondiments() {
        System.out.println("Condiments: Peanuts");
    }

    public String toString() {
        return "Bronze";
    }
}
