package Users;

import Requests.ManualRequest;
import Services.Service;

import java.awt.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import static Utils.Utils.contextLog;

/**
 * Created by hughma on 7/22/15.
 */
public class Driver extends User {
    private String __license;
    private String __insurance;
    private HashMap<String, WorkDay> __workSchedule = new HashMap<>();
    private Service __service;
    private ArrayList<Service> __queuedServices;

    public Driver(String name, String phoneNumber, String license, String insurance,
                  int startHour, int startMinute, int endHour, int endMinute){
        super();
        this._name = name;
        this._phoneNumber = phoneNumber;
        this.__license = license;
        this.__insurance = insurance;

        for (int x = 0; x <= 7; x++) {
            this.__workSchedule.put("Sunday", new WorkDay(startHour, startMinute, endHour, endMinute));
            this.__workSchedule.put("Monday", new WorkDay(startHour, startMinute, endHour, endMinute));
            this.__workSchedule.put("Tuesday", new WorkDay(startHour, startMinute, endHour, endMinute));
            this.__workSchedule.put("Wednesday", new WorkDay(startHour, startMinute, endHour, endMinute));
            this.__workSchedule.put("Thursday", new WorkDay(startHour, startMinute, endHour, endMinute));
            this.__workSchedule.put("Friday", new WorkDay(startHour, startMinute, endHour, endMinute));
            this.__workSchedule.put("Saturday", new WorkDay(startHour, startMinute, endHour, endMinute));
        }
    }

    public HashMap getWorkSchedule() {
        return this.__workSchedule;
    }

    public void setService(Service service) {
        this.__service = service;
    }

    public void addService(Service service) {
        this.__queuedServices.add(service);
    }
    public void removeService() {
        this.__service = null;
    }

	public String getLicense() {
		return __license;
	}
	
	public String getInsurance() {
		return __insurance;
	}

    public void startManualRequest(Point startLocation, Point endLocation) {
        ManualRequest manualRequest = new ManualRequest(this, startLocation, endLocation);
    }

    public void startActiveService() {
        if (__service != null ) {
            __service.startService();
        }
    }

    public void cancelActiveService() {
        if (__service != null ) {
            contextLog("Driver is Cancelling Service");
            __service.queueService();
        }
    }

    public void endActiveService() {
        if (__service != null ) {
            __service.completeService();
        }
    }

    @Override
    public void update(Service service, Object args) {
        contextLog("Updating driver on status of Service "+args.toString());
        //if (args.toString().equalsIgnoreCase("approved")) {
        //   service.startService() ;
        // }
    }
}

class WorkDay {
    private Date __startTime;
    private Date __endTime;

    public WorkDay(int startHour, int startMinute, int endHour, int endMinute) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY,startHour);
        cal.set(Calendar.MINUTE,startMinute);
        cal.set(Calendar.SECOND,0);
        cal.set(Calendar.MILLISECOND,0);

        this.__startTime = cal.getTime();

        Calendar cal2 = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, endHour);
        cal.set(Calendar.MINUTE,endMinute);
        cal.set(Calendar.SECOND,0);
        cal.set(Calendar.MILLISECOND,0);

        this.__endTime = cal2.getTime();
    }

    public Date getStartTime() {
        return this.__startTime;
    }

    public Date get__endTime() {
        return this.__endTime;
    }
}
