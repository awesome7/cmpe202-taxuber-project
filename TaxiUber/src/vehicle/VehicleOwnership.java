package vehicle;

public abstract class VehicleOwnership {
	protected String ownerName;
	public VehicleOwnership(String name){
		ownerName = name;
	}
	public String getOwnerName(){
		return ownerName;
	}
}
