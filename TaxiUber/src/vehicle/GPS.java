package vehicle;

import java.awt.Point;
import java.util.Random;

public class GPS {
	private Point location;
	
	public GPS() {
		int x = getIntInRange(0, 100);
		int y = getIntInRange(0, 100);
		location = new Point(x, y);
	}
	
	public Point getLocation() {
		return location;
	}
	
	public void setLocation(Point point) {
		location = point;
	}
	
	private int getIntInRange(int min, int max) {
		Random r = new Random();
		return r.nextInt(max - min + 1) + min;
	}
}
