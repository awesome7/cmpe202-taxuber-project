package vehicle;

import static Utils.Utils.contextLog;

/**
 * Created by hughma on 8/6/15.
 */
public class ScheduledState implements VehicleState {
    protected Vehicle _vehicle;

    public ScheduledState(Vehicle vehicle) {
        _vehicle = vehicle;
    }

    public void available() {
        _vehicle.setVehicleState(new AvailableState(_vehicle));
    }

    public void scheduledForService() {
        contextLog("Vehicle is already scheduled for service.");
    }

    public void providingService() {
        contextLog("Vehicle is beginning its service.");
        _vehicle.setVehicleState(new ProvidingServiceState(_vehicle));
    }

    public void inactive() {
        contextLog("Vehicle is already scheduled for service");
    }

    public void retired() {
        contextLog("Vehicle has to become inactive before it retires.");
    }

    public String toString() {
        return "scheduled";
    }
}