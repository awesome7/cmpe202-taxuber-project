package vehicle;

import static Utils.Utils.contextLog;

public class InactiveState implements VehicleState {
	protected Vehicle _vehicle;

	public InactiveState(Vehicle vehicle) {
		_vehicle = vehicle;
	}

	public void available() {
		_vehicle.setVehicleState(new AvailableState(_vehicle));
	}

	public void scheduledForService() {
		contextLog("Vehicle is inactive and is not serviceable.");
	}

	public void providingService() {
		contextLog("Vehicle is inactive and is not serviceable.");
	}

	public void inactive() {
		contextLog("Vehicle is already inactive");
	}

	public void retired() {
		contextLog("Vehicle is being retired.");
		_vehicle.setVehicleState(new RetiredState(_vehicle));
	}

	public String toString() {
		return "inactive";
	}
}