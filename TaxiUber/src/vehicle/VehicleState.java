package vehicle;

public interface VehicleState {
	void available();

	void scheduledForService();

	void providingService();

	void inactive();

	void retired();
}
