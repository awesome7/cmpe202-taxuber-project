package vehicle;

public class VehicleCar extends Vehicle {

	public VehicleCar (String vin, String make, String model, VehicleOwnership ownership) {
		super(vin, make, model, ownership);
		numberOfSeats = 5;
		vehicleType = "Car";
	}
}
