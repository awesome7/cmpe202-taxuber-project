package vehicle;

public class VehicleVan extends Vehicle {
	public VehicleVan (String vin, String make, String model, VehicleOwnership ownership) {
		super(vin, make, model, ownership);
		numberOfSeats = 7;
		vehicleType = "Van";
	}
}
