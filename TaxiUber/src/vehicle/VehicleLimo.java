package vehicle;

public class VehicleLimo extends Vehicle {
	public VehicleLimo (String vin, String make, String model, VehicleOwnership ownership) {
		super(vin, make, model, ownership);
		numberOfSeats = 9;
		vehicleType = "Limo";
	}
}
