package vehicle;

import static Utils.Utils.contextLog;

public class AvailableState implements VehicleState {
	protected Vehicle _vehicle;

	public AvailableState(Vehicle vehicle) {
		contextLog("Vehicle is now available for service.");
		_vehicle = vehicle;
	}

	public void available() {
		contextLog("Vehicle is available for service.");
	}

	public void scheduledForService() {
		contextLog("Vehicle is being scheduled for service.");
		_vehicle.setVehicleState(new ProvidingServiceState(_vehicle));
	}

	public void providingService() {
		contextLog("Vehicle is not providing a service.");
	}

	public void inactive() {
		contextLog("Vehicle is being set to inactive.");
		_vehicle.setVehicleState(new InactiveState(_vehicle));
	}

	public void retired() {
		contextLog("Vehicle has to become inactive before it retires.");
	}

	public String toString() {
		return "available";
	}
}
