package vehicle;

import static Utils.Utils.contextLog;

public class RetiredState implements VehicleState {

	protected Vehicle _vehicle;

	public RetiredState(Vehicle vehicle) {
		_vehicle = vehicle;
	}

	public void available() {
		contextLog("Vehicle is returning from retirement.");
		_vehicle.setVehicleState(new AvailableState(_vehicle));
	}

	public void scheduledForService() {
		contextLog("Vehicle is retired and is not serviceable.");
	}

	public void providingService() {
		contextLog("Vehicle is retired and is not serviceable.");
	}

	public void inactive() {
		contextLog("Vehicle is retired and is not serviceable.");
	}

	public void retired() {
		contextLog("Vehicle is already retired and is not serviceable.");
	}

	public String toString() {
		return "retired";
	}
}
