package vehicle;

import static Utils.Utils.contextLog;

public class ProvidingServiceState implements VehicleState{
	protected Vehicle _vehicle;

	public ProvidingServiceState(Vehicle vehicle) {
		_vehicle = vehicle;
	}

	public void available() {
		_vehicle.setVehicleState(new AvailableState(_vehicle));
	}

	public void scheduledForService() {
		contextLog("Vehicle is servicing a request.");
	}

	public void providingService() {
		contextLog("Vehicle is already servicing a request.");
	}

	public void inactive() {
		contextLog("Vehicle is servicing a request.");
	}

	public void retired() {
		contextLog("Vehicle is servicing a request.");
	}

	public String toString() {
		return "providing";
	}
}