package Tests;

import DataStore.DataStore;
import Requests.OnlineRequest;
import Users.BasicCustomer;
import Users.Customer;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by hughma on 7/28/15.
 */
public class DataStoreTest {
    public static void main(String[] args) {

        // Create the datastore
        DataStore dataStore = new DataStore();

        // Store a Customer
        Customer customer1 = new BasicCustomer("Jason", "408-555-5555");
        dataStore.put(Customer.class, customer1);
        ArrayList<Customer> customerList = dataStore.get(Customer.class);
        Iterator<Customer> customerIterator = customerList.iterator();

        // Get the customers
        while(customerIterator.hasNext()) {
            Customer currentCustomer = customerIterator.next();
            System.out.println(currentCustomer.get_name());
        }

        // Store a online request
        OnlineRequest request1 = new OnlineRequest(customer1, new Point(0,0), new Point(10,10));
        dataStore.put(OnlineRequest.class, request1);
        ArrayList<OnlineRequest> onlineRequestList = dataStore.get(OnlineRequest.class);
        Iterator<OnlineRequest> onlineIterator = onlineRequestList.iterator();

        // Get the online requests
        while(onlineIterator.hasNext()) {
            OnlineRequest currentRequest = onlineIterator.next();
            System.out.println(currentRequest.getStartPoint().toString() + " " +
                    currentRequest.getEndPoint().toString());
        }

        System.exit(0);
    }
}
