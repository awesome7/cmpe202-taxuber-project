package Tests;

import InventorySchedule.InventorySchedule;
import InventorySchedule.VehicleAndDriver;
import Requests.OnlineRequest;
import Requests.Request;
import Services.ServiceQueue;
import Users.BasicCustomer;
import Users.Customer;
import reports.CustomerReport;
import reports.InventoryReport;
import reports.ServicesReport;

import java.awt.*;
import java.util.ArrayList;

public class BaseRequest_Test {
    public static void main(String[] args) {
        InventorySchedule.getInstance().loadObjectsForTesting(1);
        printInventory();

        OnlineRequest olr1 = new OnlineRequest(new BasicCustomer("Joe", "123455"),
                new Point(1, 2), new Point(2, 3));

        ServiceQueue serviceQueue = ServiceQueue.getInstance();
        printServices();

        serviceQueue.processQueue();
        printServices();

        //Start active services
        for (VehicleAndDriver vd : InventorySchedule.getInstance().getActiveVehicleList()) {
            vd.getDriver().startActiveService() ;
        }

        //End active services
        for (VehicleAndDriver vd : InventorySchedule.getInstance().getActiveVehicleList()) {
            vd.getDriver().endActiveService() ;
        }

        //Just making a customer list....
        ArrayList<Request> requestsList = new ArrayList<Request>();
        requestsList.add(olr1);

        printCustomers(requestsList);
        printServices();
        printInventory();
    }

    public static void printServices() {
        ServicesReport servicesReport = new ServicesReport(ServiceQueue.getInstance().getServicesReportList());
        servicesReport.printReport();
    }

    public static void printInventory() {
        InventoryReport inventoryReport = new InventoryReport(InventorySchedule.getInstance().getInventoryReportList());
        inventoryReport.printReport();
    }

    public static void printCustomers(ArrayList<Request> requests) {
        for (Request req : requests) {
            CustomerReport customerReport = new CustomerReport((Customer) req.getUser());
            customerReport.printReport();
        }

    }
}
