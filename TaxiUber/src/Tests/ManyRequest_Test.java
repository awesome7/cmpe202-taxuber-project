package Tests;

import java.awt.Point;

import InventorySchedule.InventorySchedule;
import InventorySchedule.VehicleAndDriver;
import Requests.OnlineRequest;
import Services.ServiceQueue;
import Users.BasicCustomer;
import reports.InventoryReport;
import reports.ServicesReport;

public class ManyRequest_Test {
	public static void main(String[] args) {		
		InventorySchedule.getInstance().loadObjectsForTesting(6);
		printInventory();


		OnlineRequest olr1 = new OnlineRequest(new BasicCustomer("Joe", "123455"),
				new Point(1, 2), new Point(2, 3));
		OnlineRequest olr2 = new OnlineRequest(new BasicCustomer("Bob", "123455"),
				new Point(1, 2), new Point(2, 3));
		OnlineRequest olr3 = new OnlineRequest(new BasicCustomer("Jason", "123455"),
				new Point(1, 2), new Point(2, 3));
		OnlineRequest olr4 = new OnlineRequest(new BasicCustomer("Hugh", "123455"),
				new Point(1, 2), new Point(2, 3));
		OnlineRequest olr5 = new OnlineRequest(new BasicCustomer("Kara", "123455"),
				new Point(1, 2), new Point(2, 3));
		OnlineRequest olr6 = new OnlineRequest(new BasicCustomer("Shayla", "123455"),
				new Point(1, 2), new Point(2, 3));
		OnlineRequest olr7 = new OnlineRequest(new BasicCustomer("Tom", "123455"),
				new Point(1, 2), new Point(2, 3));
		OnlineRequest olr8 = new OnlineRequest(new BasicCustomer("Jijo", "123455"),
				new Point(5, 10), new Point(70, 87));
        OnlineRequest olr9 = new OnlineRequest(new BasicCustomer("Zeng", "123455"),
                new Point(1, 2), new Point(2, 3));

		ServiceQueue serviceQueue = ServiceQueue.getInstance();
		printServices();

        serviceQueue.processQueue();
		printServices();

        //Start active services
        for (VehicleAndDriver vd : InventorySchedule.getInstance().getActiveVehicleList()) {
            vd.getDriver().startActiveService() ;
        }

        //End active services
        for (VehicleAndDriver vd : InventorySchedule.getInstance().getActiveVehicleList()) {
            vd.getDriver().endActiveService() ;
        }

        printServices();
        printInventory();
	}
	
	public static void printServices() {
		ServicesReport servicesReport = new ServicesReport(ServiceQueue.getInstance().getServicesReportList());
		servicesReport.printReport();
	}
	
	public static void printInventory() {
		InventoryReport inventoryReport = new InventoryReport(InventorySchedule.getInstance().getInventoryReportList());
		inventoryReport.printReport();
	}
}
