package Tests;

import java.awt.Point;

import InventorySchedule.InventorySchedule;
import InventorySchedule.VehicleAndDriver;
import Requests.OnlineRequest;
import Services.ServiceQueue;
import Users.BasicCustomer;
import reports.InventoryReport;
import reports.ServicesReport;

public class DriverCancelTest {
	public static void main(String[] args) {		
		InventorySchedule.getInstance().loadObjectsForTesting(3);
		printInventory();

		OnlineRequest olr1 = new OnlineRequest(new BasicCustomer("jason", "123455"),
				new Point(1, 2), new Point(2, 3));

		ServiceQueue serviceQueue = ServiceQueue.getInstance();
		printServices();

        serviceQueue.processQueue();
		printServices();

		for (VehicleAndDriver vd : InventorySchedule.getInstance().getActiveVehicleList()) {
		    vd.getDriver().cancelActiveService() ;
		}

        serviceQueue.processQueue();

        for (VehicleAndDriver vd : InventorySchedule.getInstance().getActiveVehicleList()) {
            vd.getDriver().startActiveService();
            vd.getDriver().endActiveService();
        }

        printServices();
        printInventory();
	}
	
	public static void printServices() {
		ServicesReport servicesReport = new ServicesReport(ServiceQueue.getInstance().getServicesReportList());
		servicesReport.printReport();
	}
	
	public static void printInventory() {
		InventoryReport inventoryReport = new InventoryReport(InventorySchedule.getInstance().getInventoryReportList());
		inventoryReport.printReport();
	}
}
