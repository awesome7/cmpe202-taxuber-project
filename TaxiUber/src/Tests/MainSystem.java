package Tests;

import InventorySchedule.InventorySchedule;
import InventorySchedule.VehicleAndDriver;
import Requests.OnlineRequest;
import Requests.PhoneRequest;
import Requests.Request;
import Requests.RequestQueue;
import Services.Service;
import Services.ServiceQueue;
import Users.BasicCustomer;
import Users.Customer;
import Users.Driver;
import reports.*;
import vehicle.*;

import java.awt.*;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by jasroger on 8/8/15.
 */
public class MainSystem {
    public static void main(String args[]) {
        ArrayList<Customer> customers = new ArrayList<>();
        ArrayList<Driver> drivers = new ArrayList<>();
        ArrayList<Vehicle> vehicles = new ArrayList<>();
        ArrayList<Request> requests = new ArrayList<>();
        ArrayList<Service> services = new ArrayList<>();
        RequestQueue requestQueue = RequestQueue.getInstance();
        Scanner input = new Scanner(System.in);
        int choice;

        while(true) {
            String[] mainOpts = {"Create User", "Create Vehicle", "Add request", "Load Inventory",
                    "Process Request Queue", "Process Service Queue", "Drivers Start Services",
                    "Drivers Complete Services", "Delete User" };
            choice = getUserOptions("Main Menu", mainOpts);

            switch (choice) {
                case 1: // User Menu
                {
                    String[] userOpts = {"Create Customer", "Create Driver"};
                    int userChoice = getUserOptions("User Menu", userOpts);

                    switch (userChoice) {
                        case 1: // Create Customer
                        {
                            System.out.println("Please type the Customer Name");
                            String name = input.next();
                            System.out.println("Please type the Customer Phone Number");
                            String number = input.next();

                            BasicCustomer bC = new BasicCustomer(name, number);
                            customers.add(bC);
                            break;
                        }
                        case 2: // Create Driver
                        {
                            System.out.println("Please type the Driver Name");
                            String name = input.next();
                            System.out.println("Please type the Driver Phone Number");
                            String number = input.next();
                            System.out.println("Please type the Driver License Number");
                            String license = input.next();
                            System.out.println("Please type the Driver Insurance Number");
                            String insurance = input.next();

                            Driver nD = new Driver(name, number, license, insurance, 10, 0, 4, 0);
                            drivers.add(nD);
                            break;
                        }
                        default: {
                            System.out.println("Not a valid choice");
                            break;
                        }

                    }
                    break;
                }
                case 2: // Create Vehicle
                {
                    System.out.println("Is this company owned or driver owned car?");
                    String type = input.next();
                    System.out.println("What is the Vehicle Make?");
                    String make = input.next();
                    System.out.println("What is the Vehicle Model?");
                    String model = input.next();

                    VehicleOwnership ownership;

                    if (type.equalsIgnoreCase("company")) {
                        ownership = new CompanyOwned("company");
                    } else {
                        ownership = new DriverOwned("Driver");
                    }
                    Random rand = new Random(LocalTime.now().toNanoOfDay());
                    VehicleCar nV = new VehicleCar("vin#", make, model, ownership);
                    nV.setLocation((new Point(rand.nextInt(100), rand.nextInt(100))));
                    vehicles.add(nV);
                    break;
                }
                case 3: // Create Request
                {
                	if (customers.size() == 0) {
                        System.out.println("Customer information is not found. Create a customer first.");
                        break;
                	}
                    String[] userList = new String[customers.size()];
                    for (int x = 0; x < customers.size(); x++) {
                        userList[x] = customers.get(x).get_name();
                    }
                    int customerIndex = getUserOptions("Which Customer will make this request?", userList);
                    customerIndex--;

                    String[] requestOpts = {"Create Online Request", "Create Phone Request"};
                    int requestChoice = getUserOptions("Request Menu", requestOpts);

                    System.out.println("What is your X coordinate");
                    int fromX = input.nextInt();
                    System.out.println("What is your Y coordinate");
                    int fromY = input.nextInt();

                    System.out.println("What is your destination X coordinate");
                    int toX = input.nextInt();
                    System.out.println("What is your destination Y coordinate");
                    int toY = input.nextInt();

                    switch (requestChoice) {
                        case 1: // Online Req
                        {
                            OnlineRequest onlineRequest = new OnlineRequest(customers.get(customerIndex),
                                    new Point(fromX, fromY), new Point(toX, toY));
                            requests.add(onlineRequest);
                            requestQueue.addRequest(onlineRequest);
                            break;
                        }
                        case 2: // Phone Req
                        {
                            PhoneRequest phoneRequest = new PhoneRequest(customers.get(customerIndex),
                                    new Point(fromX, fromY), new Point(toX, toY));
                            requests.add(phoneRequest);
                            requestQueue.addRequest(phoneRequest);
                            break;
                        }
                        default: {
                            System.out.println("Not a valid choice");
                            break;
                        }
                    }
                    break;
                }
                case 4: //Load Inventory
                {
                    if (vehicles.size() < 1 && drivers.size() < 1) {
                        System.out.println("No vehicles and Drivers have been created, cannot load inventory");
                        System.out.println("Would you like to auto-generate and load driver and vehicles? (yes/no)");
                        String genAnswer = input.next();
                        if (genAnswer.equalsIgnoreCase("yes")) {
                            System.out.println("How Many?");
                            int numAns = input.nextInt();
                            InventorySchedule.getInstance().loadObjectsForTesting(numAns);
                        } else {
                            System.out.println("no driver or vehicles, returning to main menu");
                        }
                        break;
                    } else {
                        int lcd = 0;
                        if (vehicles.size() < drivers.size()) {
                            lcd = vehicles.size();
                        } else {
                            lcd = drivers.size();
                        }

                        for (int x = 0; x < lcd; x++) {
                            VehicleAndDriver vd = new VehicleAndDriver();
                            vd.setDriver(drivers.get(x));
                            vd.setVehicle(vehicles.get(x));
                            InventorySchedule.getInstance().addVehicleAndDriver(vd);
                            if (vd.getVehicle().getVehicleOwnership().getOwnerName().equalsIgnoreCase("company")) {
                                InventorySchedule.getInstance().addVehicle(vd.getVehicle());
                                InventorySchedule.getInstance().reportAddCompanyOwnedInventory(vd);
                            } else {
                                InventorySchedule.getInstance().reportAddDriverOwnedInventory(vd);
                            }

                        }
                    }
                    break;
                }
                
                case 9:
                { // delete user
                	if (customers != null && customers.size() > 0){
                    	String userName = promptUser("Name of the user to delete:");
                    	boolean deleted = false;
                    	for (int i=0; i<customers.size(); i++){
                    		if (customers.get(i).get_name().equalsIgnoreCase(userName)){                			
                    			System.out.println("Customer " + customers.get(i).get_name() + " is deleted");
                    			customers.remove(i);
                    			deleted = true;
                    			break;
                    		}
                    	}
                    	if (!deleted){
                    		System.out.println("Customer not found");
                    	}                		
                	} else {
                		System.out.println("Customers not found");
                	}
                	break;
                }
                
                default: {
                    System.out.println("Not a valid choice");
                    break;
                }
                case 5: //Process Request Queue
                {
                    requestQueue.processQueue();
                    for (Request r : requests) {
                        if (r.getService() != null) {
                            services.add(r.getService());
                        }
                    }
                    break;
                }
                case 6: //Process Service Queue
                {
                    ServiceQueue.getInstance().processQueue();
                    break;
                }
                case 7: //Drivers Start Scheduled Services
                {
                    for (VehicleAndDriver vd : InventorySchedule.getInstance().getActiveVehicleList()) {
                        vd.getDriver().startActiveService();
                    }
                    break;
                }
                case 8: //Drivers Complete Scheduled Services
                {
                    for (VehicleAndDriver vd : InventorySchedule.getInstance().getActiveVehicleList()) {
                        vd.getDriver().endActiveService();
                    }
                    break;
                }
            }
            printReports(customers, vehicles, drivers, services);
            requestQueue.printContents();
            //printServices();
            //printInventory();
        }

    }

    public static void printReports(ArrayList<Customer> cust, ArrayList<Vehicle> veh, ArrayList<Driver> driv, ArrayList<Service> serv) {
        Report rep;
        for (Customer customer : cust) {
            rep = new CustomerReport(customer);
            rep.printReport();
        }
        for (Vehicle vehicle : veh) {
            rep = new VehicleReport(vehicle);
            rep.printReport();
        }
        for (Driver driver : driv) {
            rep = new DriverReport(driver);
            rep.printReport();
        }
        for (Service service : serv) {
            rep = new ServiceReport(service);
            rep.printReport();
        }
    }

    public static String promptUser(String prompt) {
        System.out.println(prompt + ":");
        Scanner input = new Scanner(System.in);
        return input.next();
    }

    public static int getUserOptions(String title, String[] options) {
        System.out.println(title);
        for (int i = 0; i < options.length; i++) {
            System.out.println(i + 1 + ") " + options[i]);
        }
        Scanner input = new Scanner(System.in);
        int choice = Integer.parseInt(input.next());
        return choice;
    }

    public static void testMenu() {
        String[] options = {"Create User", "Delete User"};
        System.out.println("User selected:" + getUserOptions("Main Menu", options));
    }
    public static void printServices() {
        ServicesReport servicesReport = new ServicesReport(ServiceQueue.getInstance().getServicesReportList());
        servicesReport.printReport();
    }

    public static void printInventory() {
        InventoryReport inventoryReport = new InventoryReport(InventorySchedule.getInstance().getInventoryReportList());
        inventoryReport.printReport();
    }

}
