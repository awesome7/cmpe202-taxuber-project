package Tests;

import Requests.OnlineRequest;
import Requests.Request;
import Users.BasicCustomer;
import Users.User;

import java.awt.*;

/**
 * Created by hughma on 7/27/15.
 */
public class RequestTest {
    public static void main(String[] args) {
        User user1 = new BasicCustomer("Jason", "408-555-5555");
        Point start = new Point(0,0);
        Point end = new Point(30,5);

        Request req = new OnlineRequest(user1, start, end);
        System.out.println(req);
        System.out.println(req.getRequestState().toString());
    }
}
