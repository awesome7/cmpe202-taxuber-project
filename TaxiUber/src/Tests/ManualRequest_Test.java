package Tests;

import java.awt.Point;

import InventorySchedule.InventorySchedule;
import InventorySchedule.VehicleAndDriver;
import Requests.OnlineRequest;
import Services.ServiceQueue;
import Users.BasicCustomer;
import reports.InventoryReport;
import reports.ServicesReport;

public class ManualRequest_Test {
    public static void main(String[] args) {
        InventorySchedule.getInstance().loadObjectsForTesting(6);
        printInventory();

        VehicleAndDriver roamingVD = InventorySchedule.getInstance().getFreeVehicleAndDriver();
        if(roamingVD != null) {
            int dumbX = ((int) roamingVD.getVehicle().getLocation().getX()) + 5;
            int dumbY = ((int) roamingVD.getVehicle().getLocation().getY()) + 5;
            roamingVD.getDriver().startManualRequest(roamingVD.getVehicle().getLocation(),
                    new Point(dumbX, dumbY));
        }

        ServiceQueue serviceQueue = ServiceQueue.getInstance();
        printServices();

        serviceQueue.processQueue();
        printServices();

        //Start active services
        for (VehicleAndDriver vd : InventorySchedule.getInstance().getActiveVehicleList()) {
            vd.getDriver().startActiveService() ;
        }

        //End active services
        for (VehicleAndDriver vd : InventorySchedule.getInstance().getActiveVehicleList()) {
            vd.getDriver().endActiveService() ;
        }

        printServices();
        printInventory();
    }

    public static void printServices() {
        ServicesReport servicesReport = new ServicesReport(ServiceQueue.getInstance().getServicesReportList());
        servicesReport.printReport();
    }

    public static void printInventory() {
        InventoryReport inventoryReport = new InventoryReport(InventorySchedule.getInstance().getInventoryReportList());
        inventoryReport.printReport();
    }
}
