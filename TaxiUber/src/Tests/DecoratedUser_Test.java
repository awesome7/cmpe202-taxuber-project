package Tests;

import InventorySchedule.InventorySchedule;
import InventorySchedule.VehicleAndDriver;
import Requests.OnlineRequest;
import Services.ServiceQueue;
import Users.*;
import reports.CustomerReport;
import reports.InventoryReport;
import reports.Report;
import reports.ServicesReport;
import Utils.Utils;

import java.awt.*;
import java.util.Random;

public class DecoratedUser_Test {
    public static void main(String[] args) {
        InventorySchedule.getInstance().loadObjectsForTesting(2);
        printInventory();

        Customer joe = new BasicCustomer("Joe", "1234567");
        joe = new BronzeDecorator(joe);
        Customer amy = new BasicCustomer("Amy", "1234567");
        amy = new SilverDecorator(amy);
        Customer jason = new BasicCustomer("Jason", "12345");
        jason = new GoldDecorator(jason);

        Random r = new Random();
        int scale = r.nextInt(20);
        OnlineRequest olr1 = new OnlineRequest(joe,
                new Point(r.nextInt(5)*scale+1, r.nextInt(5)*scale+1),
                new Point(r.nextInt(5)*scale+1, r.nextInt(5)*scale+1));
        OnlineRequest olr2 = new OnlineRequest(amy,
                new Point(r.nextInt(5)*scale+1, r.nextInt(5)*scale+1),
                new Point(r.nextInt(5)*scale+1, r.nextInt(5)*scale+1));
        OnlineRequest olr3 = new OnlineRequest(jason,
                new Point(r.nextInt(5)*scale+1, r.nextInt(5)*scale+1),
                new Point(r.nextInt(5)*scale+1, r.nextInt(5)*scale+1));

        ServiceQueue serviceQueue = ServiceQueue.getInstance();
        printServices();

        serviceQueue.processQueue();
        printServices();

        //Start active services
        for (VehicleAndDriver vd : InventorySchedule.getInstance().getActiveVehicleList()) {
            vd.getDriver().startActiveService();
        }

        //End active services
        for (VehicleAndDriver vd : InventorySchedule.getInstance().getActiveVehicleList()) {
            vd.getDriver().endActiveService();
        }

        printServices();
        printInventory();
        Report custRep = new CustomerReport(jason);
        custRep.printReport();
    }

    public static void printServices() {
        ServicesReport servicesReport = new ServicesReport(ServiceQueue.getInstance().getServicesReportList());
        servicesReport.printReport();
    }

    public static void printInventory() {
        InventoryReport inventoryReport = new InventoryReport(InventorySchedule.getInstance().getInventoryReportList());
        inventoryReport.printReport();
    }
}
