package Tests;

import Users.*;

/**
 * Created by hughma on 7/30/15.
 */
public class UserTest {
    public static void main(String[] args) {
//        Driver driver = new Driver("John Smith", "408-123-3456", "AZA123", "Geico");
//        System.out.println("Driver Created: " + driver.get_name());

        Customer customer = new BasicCustomer("Jerry Tom", "999-999-9999");
        System.out.println("BasicCustomer Created: " + customer.get_name());

        System.out.println("\nBronze Status!");
        customer = new BronzeDecorator(customer);
        customer.checkBenefits();

        System.out.println("\nSilver Status!");
        customer = new SilverDecorator(customer);
        customer.checkBenefits();

        System.out.println("\nGold Status!");
        customer = new GoldDecorator(customer);
        customer.checkBenefits();
    }
}
